# Change Log
All notable changes to the "region-marker" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [1.2.1]
- Fixed broken `Region Marker: Mark Region`

## [1.2.0]
- Added support for SCSS and HTML files, thanks to @jerone
- Named end-regions are now supported via `region-marker.namedEndRegion` (Can be set per language)

## [1.1.0]
- Added support for tsx and jsx files, thanks to Thomas Müller (@tmu)

## [1.0.0]
- Initial release
- Provides `Region Marker: Mark Region` and `Region Marker: Mark Named Region` commands
- Support for customizing region delimiters via settings