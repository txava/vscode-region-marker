# Region Marker

Region Marker allows you define code regions quickly.

## Features

Mark code regions selecting a chunk of code and running the `Region Marker: Mark Region` command.
> Tip: Invoking the command with no selection will create a region on the cursor position line.

![Region Marker: Mark Region](https://gitlab.com/txava/vscode-region-marker/-/raw/master/images/mark-region.gif)

---

Also, you can mark regions of code with a name by running the `Region Marker: Mark Named Region` command.

![Region Marker: Mark Named Region](https://gitlab.com/txava/vscode-region-marker/-/raw/master/images/mark-named-region.gif)


### Out of the box supported languages
* JavaScript
* TypeScript
* Ruby
* C#
* C
* C++
* Powershell
* Visual Basic
* JSX & TSX
* SCSS
* HTML

> Support for other languages can be added by adding region limiters via settings `region-marker.languages`

## Extension Settings

This extension contributes the following settings:

* `region-marker.languages`: Customizes the region delimiters for languages

```json
"region-marker.languages": {
     // Language id
    "typescript": {
        "start": "// my-custom-region ",
        "end": "// end-of-custom-region"
    }
}
```

Will make `TypeScript` regions to be like:

![Custom delimiter example](https://gitlab.com/txava/vscode-region-marker/-/raw/master/images/custom-delimiter.png)

* `region-marker.namedEndRegion` _(boolean)_: Adds the the name of the region to the end limiter.

![Named end region example](https://gitlab.com/txava/vscode-region-marker/-/raw/master/images/named-endregion.png)

> It can be set at language level too:

```json
"[typescript]": {
    "region-marker.namedEndRegion": true
}
```
