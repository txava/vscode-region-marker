import * as vscode from 'vscode';

import { DefaultLanguageRegionDelimitersConfig, getRegionDelimiters, LanguageDelimitersConfig } from './common';

const REGION_NAME_TEMPLATE_VAR = '${regionName}';

export function hasInterpolatedRegionName(delimiter: string): boolean {
    return delimiter.indexOf(REGION_NAME_TEMPLATE_VAR) !== -1;
}

export class RegionMarkerManager {
    private languageDelimiters: LanguageDelimitersConfig = {};
    private disposables: vscode.Disposable[] = [];
    private namedEndRegion = false;

    constructor() {
        this.disposables.push(
            vscode.workspace.onDidChangeConfiguration(this.handleDidChangeConfiguration.bind(this))
        );

        this.updateConfiguration();
    }

    registerCommands() {
        this.disposables.push(vscode.commands.registerCommand('region-marker.markRegion',  () => {
            const activeTextEditor = vscode.window.activeTextEditor;
            if (activeTextEditor) {
                this.markRegion(activeTextEditor);
            }
        }));

        this.disposables.push(
            vscode.commands.registerCommand('region-marker.markNamedRegion', async () => {
                const regionName = await vscode.window.showInputBox({
                    prompt: 'Region name',
                });
                const activeTextEditor = vscode.window.activeTextEditor;
                if (activeTextEditor) {
                    const languageId = {
                        languageId: 'typescript',
                    };
                    const namedEndRegion = vscode.workspace.getConfiguration('', languageId).get<boolean>('region-marker.namedEndRegion');
                    // const namedEndRegion = vscode.workspace.getConfiguration('', activeTextEditor.document.uri).get<boolean>('region-marker.namedEndRegion');
                    this.markRegion(activeTextEditor, regionName, namedEndRegion);
                }
            })
        );
    }

    handleDidChangeConfiguration(event: vscode.ConfigurationChangeEvent) {
        if (event.affectsConfiguration('region-marker')) {
            this.updateConfiguration();
        }
    }

    initializeDelimitersConfig() {
        this.languageDelimiters = {...DefaultLanguageRegionDelimitersConfig };
    }

    updateConfiguration() {
        this.initializeDelimitersConfig();
        this.namedEndRegion = vscode.workspace.getConfiguration('region-marker', null).get('namedEndRegion') as boolean;

        let languagesConfig = vscode.workspace.getConfiguration('region-marker', null).get('languages') as LanguageDelimitersConfig;
        if (languagesConfig) {
            Object.keys(languagesConfig).forEach(language => {
                this.languageDelimiters[language] = languagesConfig[language];
            });
        }
    }

    markRegion(editor: vscode.TextEditor, name?: string, namedEndRegion: boolean = false) {
        const activeTextEditor = editor;
        if (activeTextEditor) {
            const regionDelimiter = getRegionDelimiters(activeTextEditor.document.languageId, this.languageDelimiters);
            let initialRegionDelimiter = regionDelimiter.start;

            if (name) {
                if (hasInterpolatedRegionName(initialRegionDelimiter)) {
                    initialRegionDelimiter = initialRegionDelimiter.replace(REGION_NAME_TEMPLATE_VAR, ` ${name} `);
                } else {
                    initialRegionDelimiter += ` ${name}`;
                }
            } else {
                if (hasInterpolatedRegionName(initialRegionDelimiter)) {
                    initialRegionDelimiter = initialRegionDelimiter.replace(REGION_NAME_TEMPLATE_VAR, ` `);
                }
            }


            activeTextEditor.edit((editBuilder) => {
                let firstValidLine = undefined;
                let lastValidLine = undefined;

                let primarySelection = activeTextEditor.selection;
                for (let i = primarySelection.start.line; i <= primarySelection.end.line; i++) {
                    const line = activeTextEditor.document.lineAt(i);
                    if (!line.isEmptyOrWhitespace) {
                        if (firstValidLine === undefined) {
                            firstValidLine = line;
                        }

                        lastValidLine = line;
                    }
                }

                if (firstValidLine && lastValidLine) {
                    const indentationRange = new vscode.Range(
                        firstValidLine.lineNumber,
                        0,
                        firstValidLine.lineNumber,
                        firstValidLine.firstNonWhitespaceCharacterIndex
                    );
                    const indentationText = activeTextEditor.document.getText(indentationRange);
                    let endRegionDelimiter = `\n${indentationText}${regionDelimiter.end}`;
                    if (name && namedEndRegion) {
                        if (hasInterpolatedRegionName(endRegionDelimiter)) {
                            endRegionDelimiter = endRegionDelimiter.replace(REGION_NAME_TEMPLATE_VAR, ` ${name} `);
                        } else {
                            endRegionDelimiter += ` ${name}`;
                        }
                    } else {
                        if (hasInterpolatedRegionName(endRegionDelimiter)) {
                            endRegionDelimiter = endRegionDelimiter.replace(REGION_NAME_TEMPLATE_VAR, ' ');
                        }
                    }

                    editBuilder.insert(firstValidLine.range.start.with(undefined, 0),  `${indentationText}${initialRegionDelimiter}\n`);
                    editBuilder.insert(lastValidLine.range.end, endRegionDelimiter);
                }
            });
        }
    }

    async markNamedRegion() {
        const regionName = await vscode.window.showInputBox({
            prompt: 'Region name',
        });

        if (regionName) {
            const activeTextEditor = vscode.window.activeTextEditor;
            if (activeTextEditor) {
                this.markRegion(activeTextEditor, regionName, this.namedEndRegion);
            }

        }
    }

    dispose() {
        this.disposables.forEach(disposable => disposable.dispose());
    }
}
