export const DefaultLanguageRegionDelimitersConfig = {
    js: {
        start: '//#region',
        end: '//#endregion',
    },
    javascriptreact: {
        start: '//#region',
        end: '//#endregion',
    },
    typescriptreact: {
        start: '//#region',
        end: '//#endregion',
    },
    typescript: {
        start: '//#region',
        end: '//#endregion',
    },
    ruby: {
        start: '#region',
        end: '#endregion',
    },
    csharp: {
        start: '#region',
        end: '#endregion',
    },
    c: {
        start: '#pragma region',
        end: '#pragma endregion',
    },
    cpp: {
        start: '#pragma region',
        end: '#pragma endregion',
    },
    powershell: {
        start: '#region',
        end: '#endregion'
    },
    vb: {
        start: '#Region',
        end: '#End Region',
    },
    scss: {
        start: '//#region',
        end: '//#endregion',
    },
    html: {
        start: "<!--${regionName}-->",
        end: "<!--//${regionName}-->",

    },
    default: {
        start: '//#region',
        end: '//#region',
    },
};

export interface RegionDelimiters {
    start: string;
    end: string;
}

export interface LanguageDelimitersConfig {
    [key:string]: RegionDelimiters;
}


export function getRegionDelimiters(languageId: string, languageDelimitersConfig: LanguageDelimitersConfig): RegionDelimiters {
    const regionDelimiters = languageDelimitersConfig[languageId] || languageDelimitersConfig['default'];
    return regionDelimiters;
}