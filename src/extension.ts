'use strict';

import * as vscode from 'vscode';

import { RegionMarkerManager } from './RegionMarkerManager';

export function activate(context: vscode.ExtensionContext) {
    const regionManager = new RegionMarkerManager();
    context.subscriptions.push(regionManager);
    regionManager.registerCommands();
}

export function deactivate() {
    // no-op
}