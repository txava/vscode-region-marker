/* eslint-disable import/prefer-default-export */
import path = require('path');
import Mocha = require('mocha');
import glob = require('glob');

export function run(): Promise<void> {
  // eslint-disable-next-line global-require
  require('source-map-support').install();

  // Create the mocha test
  const mocha = new Mocha({
    ui: 'bdd',
    reporter: 'min',
    timeout: 10000,
  });
  mocha.useColors(true);

  const testsRoot = path.resolve(__dirname);

  return new Promise((c, e) => {
    // eslint-disable-next-line consistent-return
    glob('**/**.test.js', { cwd: testsRoot }, (err, files) => {
      if (err) {
        return e(err);
      }

      // Add files to the test suite
      files.forEach(f => mocha.addFile(path.resolve(testsRoot, f)));

      try {
        // Run the mocha test
        mocha.run(failures => {
          if (failures > 0) {
            e(new Error(`${failures} tests failed.`));
          } else {
            c();
          }
        });
      } catch (mochaRunErr) {
        e(mochaRunErr);
      }
    });
  });
}
