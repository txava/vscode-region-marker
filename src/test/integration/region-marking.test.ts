import * as vscode from 'vscode';
import path = require('path');
import chai = require('chai');
import { RegionMarkerManager } from '../../RegionMarkerManager';

const { expect } = chai;

describe('Region Marking', function () {
    beforeEach(async function () {
        return vscode.commands.executeCommand('workbench.action.closeActiveEditor').then(() => {
            return new Promise(resolve => setTimeout(resolve, 2000));
        });
    });

    it('Named end-regions', async function () {
        if(!vscode.workspace.workspaceFolders) {
            throw new Error('No workspace folders');
        }

        const documentURI = vscode.workspace.workspaceFolders[0].uri.with({
            path: path.resolve(vscode.workspace.workspaceFolders[0].uri.path, 'test.scss'),
        });
        const document = await vscode.workspace.openTextDocument(documentURI);
        const textEditor = await vscode.window.showTextDocument(document);

        await vscode.commands.executeCommand('cursorTop').then(() => {
            return new Promise(resolve => setTimeout(resolve, 1000));
        });

        const manager = new RegionMarkerManager();
        manager.markRegion(textEditor, 'my-region', true);

        await new Promise(resolve => setTimeout(resolve, 1000));
        
        const startRegion = document.lineAt(0);
        const endRegion = document.lineAt(2);

        expect(startRegion.text).to.be.equals('//#region my-region');
        expect(endRegion.text).to.be.equals('//#endregion my-region');
    });

    it('Support for HTML', async function () {
        if(!vscode.workspace.workspaceFolders) {
            throw new Error('No workspace folders');
        }

        const documentURI = vscode.workspace.workspaceFolders[0].uri.with({
            path: path.resolve(vscode.workspace.workspaceFolders[0].uri.path, 'test.html'),
        });
        const document = await vscode.workspace.openTextDocument(documentURI);
        const textEditor = await vscode.window.showTextDocument(document);

        await vscode.commands.executeCommand('cursorTop').then(() => {
            return new Promise(resolve => setTimeout(resolve, 1000));
        });

        const manager = new RegionMarkerManager();
        manager.markRegion(textEditor);

        await new Promise(resolve => setTimeout(resolve, 1000));
        
        const startRegion = document.lineAt(0);
        const endRegion = document.lineAt(2);

        expect(startRegion.text).to.be.equals('<!-- -->');
        expect(endRegion.text).to.be.equals('<!--// -->');
    });

    it('Support for HTML with named end regions', async function () {
        if(!vscode.workspace.workspaceFolders) {
            throw new Error('No workspace folders');
        }

        const documentURI = vscode.workspace.workspaceFolders[0].uri.with({
            path: path.resolve(vscode.workspace.workspaceFolders[0].uri.path, 'test.html'),
        });
        const document = await vscode.workspace.openTextDocument(documentURI);
        const textEditor = await vscode.window.showTextDocument(document);

        await vscode.commands.executeCommand('cursorTop').then(() => {
            return new Promise(resolve => setTimeout(resolve, 1000));
        });

        const manager = new RegionMarkerManager();
        manager.markRegion(textEditor, 'my-region', true);

        await new Promise(resolve => setTimeout(resolve, 1000));
        
        const startRegion = document.lineAt(0);
        const endRegion = document.lineAt(2);

        expect(startRegion.text).to.be.equals('<!-- my-region -->');
        expect(endRegion.text).to.be.equals('<!--// my-region -->');
    });
    
});
