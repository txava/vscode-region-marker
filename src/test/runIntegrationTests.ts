import { runTests } from 'vscode-test';

import path = require('path');

async function main(): Promise<void> {
  try {
    // The folder containing the Extension Manifest package.json
    // Passed to `--extensionDevelopmentPath`
    const extensionDevelopmentPath = path.resolve(__dirname, '../../');

    // The path to test runner
    // Passed to --extensionTestsPath
    const extensionTestsPath = path.resolve(__dirname, './integration/index');
    const testWorkspace = path.resolve(__dirname, '../../test-fixtures/fixture1');
    console.log(testWorkspace);
    


    // Download VS Code, unzip it and run the integration test
    await runTests({
      extensionDevelopmentPath,
      extensionTestsPath,
      launchArgs: ['--disable-extensions', testWorkspace],
      version: '1.42.0',
    });
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error('Failed to run tests');
    process.exit(1);
  }
}

main();
